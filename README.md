## Sobre

Conteúdo do **Minicurso: introdução à Relatividade Geral**, ministrado
durante o programa de verão/2018 no PPGMAT/UFAL.

## Bibliografia

Siga o link para as [referências bibliográficas](https://www.zotero.org/groups/1797153/introduo__relatividade_geral/items).

## Licença

O conteúdo do projeto em si está licenciado sob os termos da
[licença Creative Commons Atribuição 4.0](https://creativecommons.org/licenses/by/4.0/deed.pt_BR),
e o código subjacente usado para formatar e apresentar o conteúdo está
licenciado sob os termos da [licença GNU AGPL-3.0+](./LICENSE.AGPL-3.0.md).
